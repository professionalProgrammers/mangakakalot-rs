/// Chapter types
pub mod chapter;
/// home_json_search types
pub mod home_json_search;
/// Manga types
pub mod manga;

pub use self::chapter::Chapter;
pub use self::home_json_search::HomeJsonSearchEntry;
pub use self::manga::Manga;
