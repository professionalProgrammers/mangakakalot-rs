/// The client
pub mod client;
/// API types
pub mod types;

pub use self::client::Client;
pub use self::types::HomeJsonSearchEntry;
pub use self::types::Manga;
pub use crate::types::Chapter;
pub use scraper::Html;

/// The library error type
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// A reqwest error
    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),

    /// A tokio task failed
    #[error(transparent)]
    TokioJoin(#[from] tokio::task::JoinError),

    /// The Manga is invalid
    #[error("invalid manga")]
    InvalidManga(#[from] self::types::manga::FromHtmlError),

    /// The Chapter is invalid
    #[error("invalid chapter")]
    InvalidChapter(#[from] self::types::chapter::FromHtmlError),
}

#[cfg(test)]
mod test {
    use super::*;

    #[tokio::test]
    async fn search() {
        let client = Client::new();
        let res = client.search("maid").await.expect("failed to search");
        dbg!(res);
    }

    #[tokio::test]
    async fn get_manga() {
        let client = Client::new();
        let url = "https://mangakakalot.com/read-vs1og158504964758";
        let res = client.get_manga(url).await.expect("failed to get manga");
        dbg!(res);
    }

    #[tokio::test]
    async fn get_chapter() {
        let client = Client::new();
        let url = "https://mangakakalot.com/chapter/ob920615/chapter_1";
        let res = client
            .get_chapter(url)
            .await
            .expect("failed to get chapter");
        dbg!(res);
    }
}
