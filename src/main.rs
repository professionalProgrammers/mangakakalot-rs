use anyhow::Context;
use std::path::Path;
use url::Url;

#[derive(argh::FromArgs)]
#[argh(description = "A tool to download from mangakakalot")]
pub struct Options {
    #[argh(subcommand)]
    subcommand: SubCommand,
}

#[derive(argh::FromArgs)]
#[argh(subcommand)]
enum SubCommand {
    Search(SearchOptions),
    Download(DownloadOptions),
}

#[derive(argh::FromArgs)]
#[argh(
    subcommand,
    description = "search for manga using a query",
    name = "search"
)]
pub struct SearchOptions {
    #[argh(positional, description = "the search query")]
    query: String,
}

#[derive(argh::FromArgs)]
#[argh(subcommand, description = "download a manga", name = "download")]
pub struct DownloadOptions {
    #[argh(positional, description = "the manga url")]
    url: Url,
}

fn main() {
    let options: Options = argh::from_env();
    let code = match real_main(options) {
        Ok(()) => 0,
        Err(e) => {
            eprintln!("{:?}", e);
            1
        }
    };

    std::process::exit(code);
}

fn real_main(options: Options) -> anyhow::Result<()> {
    let tokio_rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .context("failed to create tokio runtime")?;

    tokio_rt.block_on(async_main(options))?;

    Ok(())
}

async fn async_main(options: Options) -> anyhow::Result<()> {
    let client = mangakakalot::Client::new();

    match options.subcommand {
        SubCommand::Search(options) => {
            let results = client
                .search(&options.query)
                .await
                .context("failed to search")?;

            println!("Got {} results for '{}'", results.len(), options.query);
            for (i, result) in results.iter().enumerate() {
                println!("{}) {}", i + 1, result.name);
                println!("ID: {}", result.id);
                println!("Name Unsigned: {}", result.name_unsigned);
                println!("Last Chapter: {}", result.last_chapter);
                println!("Image: {}", result.image);
                println!("Author: {}", result.author);
                println!("Story Link: {}", result.story_link);
                println!();
            }
        }
        SubCommand::Download(options) => {
            println!("Getting manga...");
            let manga = client
                .get_manga(options.url.as_str())
                .await
                .context("failed to get manga")?;

            let out_dir = Path::new(".");

            println!("Title: {}", manga.title);
            println!("Out Dir: {}", out_dir.display());
            println!();

            for (i, chapter) in manga.chapter_list.iter().rev().enumerate() {
                println!("Downloading '{}' ({})...", chapter.title, i + 1);
                let mut chapter_url_iter =
                    chapter.url.path_segments().context("missing path")?.rev();
                let chapter_dir_name = chapter_url_iter
                    .next()
                    .context("missing last path segment")?;
                let manga_name = chapter_url_iter
                    .next()
                    .context("missing manga name segment")?;
                let chapter_path = out_dir.join(manga_name).join(chapter_dir_name);

                tokio::fs::create_dir_all(&chapter_path)
                    .await
                    .context("failed to create chapter path")?;
                println!("    Saving to '{}'", chapter_path.display());

                let chapter = client
                    .get_chapter(chapter.url.as_str())
                    .await
                    .context("failed to get chapter")?;

                for (j, image) in chapter.images.iter().enumerate() {
                    println!("    Downloading '{}' ({})...", image.as_str(), j + 1);
                    let image_name = image
                        .path_segments()
                        .context("missing path")?
                        .rev()
                        .next()
                        .context("missing image name")?;
                    let image_path = chapter_path.join(image_name);

                    println!("        Saving to '{}' ...", image_path.display());
                    if image_path.exists() {
                        println!("        File exists, skipping...");
                    } else {
                        let bytes = client
                            .client
                            .get(image.as_str())
                            .header(reqwest::header::REFERER, "https://mangakakalot.com/")
                            .send()
                            .await?
                            .error_for_status()?
                            .bytes()
                            .await?;
                        tokio::fs::write(image_path, bytes)
                            .await
                            .context("failed to save")?;
                    }
                }

                println!();
            }
        }
    }
    Ok(())
}
