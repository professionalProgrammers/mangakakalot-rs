use crate::Chapter;
use crate::Error;
use crate::HomeJsonSearchEntry;
use crate::Manga;
use scraper::Html;

/// A mangakakalot client
#[derive(Debug, Clone)]
pub struct Client {
    /// The inner http client
    ///
    /// It probably shouldn't be used by you
    pub client: reqwest::Client,
}

impl Client {
    /// Make a new [`Client`].
    pub fn new() -> Self {
        Self {
            client: reqwest::Client::new(),
        }
    }

    /// Search with a query
    pub async fn search(&self, query: &str) -> Result<Vec<HomeJsonSearchEntry>, Error> {
        // query uses _ for spaces. Multiple spaces between words become one space.
        let mut query: String = query
            .split(' ')
            .filter(|s| !s.is_empty())
            .map(|el| el.chars().chain(std::iter::once('_')))
            .flatten()
            .collect();
        if query.ends_with('_') {
            query.pop();
        }

        Ok(self
            .client
            .post("https://mangakakalot.com/home_json_search")
            .form(&[("searchword", query.as_str())])
            .send()
            .await?
            .error_for_status()?
            .json()
            .await?)
    }

    /// Send a Get to the url and get the response as text.
    pub async fn get_text(&self, url: &str) -> Result<String, Error> {
        Ok(self
            .client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .text()
            .await?)
    }

    /// Send a Get to the url and get the response as html, using the transform function to transform it into a type.
    pub async fn get_html<T, R>(&self, url: &str, t: T) -> Result<R, Error>
    where
        T: FnOnce(Html) -> R + Send + 'static,
        R: Send + 'static,
    {
        let text = self.get_text(url).await?;
        Ok(tokio::task::spawn_blocking(move || {
            let html = Html::parse_document(&text);
            t(html)
        })
        .await?)
    }

    /// Get a Manga from a url
    pub async fn get_manga(&self, url: &str) -> Result<Manga, Error> {
        Ok(self.get_html(url, |html| Manga::from_html(&html)).await??)
    }

    /// Get a Chapter from a url
    pub async fn get_chapter(&self, url: &str) -> Result<Chapter, Error> {
        Ok(self
            .get_html(url, |html| Chapter::from_html(&html))
            .await??)
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}
