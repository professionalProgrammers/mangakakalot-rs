use std::collections::HashMap;
use url::Url;

/// A json search result entry
#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct HomeJsonSearchEntry {
    /// The comic id
    pub id: String,
    /// The "pretty" name.
    ///
    /// This usually is html
    pub name: String,

    /// The internal name?
    #[serde(rename = "nameunsigned")]
    pub name_unsigned: String,

    /// The last chapter?
    #[serde(rename = "lastchapter")]
    pub last_chapter: String,

    /// The image url
    pub image: Url,

    /// The author
    ///
    /// This is usually html
    pub author: String,

    /// The url
    pub story_link: Url,

    /// Unknown data
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

#[cfg(test)]
mod test {
    use super::*;

    const MAID_RESULT: &str = include_str!("../../test_data/home_json_search_maid.json");

    #[test]
    fn parse_maid() {
        let res: Vec<HomeJsonSearchEntry> =
            serde_json::from_str(MAID_RESULT).expect("failed to parse maid");
        dbg!(res);
    }
}
