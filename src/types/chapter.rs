use scraper::Html;
use scraper::Selector;
use url::Url;

/// Error that may occur while parsing a [`Chapter`] from [`Html`].
#[derive(Debug, thiserror::Error)]
pub enum FromHtmlError {
    /// Missing an image url
    #[error("missing image url")]
    MissingImageUrl,

    /// The image url is invalid
    #[error("invalid image url")]
    InvalidImageUrl(url::ParseError),
}

/// A Manga chapter
#[derive(Debug)]
pub struct Chapter {
    /// Chapter images
    pub images: Vec<Url>,
}

impl Chapter {
    /// Make a [`Chapter`] from [`Html`]
    pub fn from_html(html: &Html) -> Result<Self, FromHtmlError> {
        lazy_static::lazy_static! {
            static ref IMAGES_SELECTOR: Selector = Selector::parse(".container-chapter-reader > img").expect("invalid images selector");
        }

        let images = html
            .select(&IMAGES_SELECTOR)
            .map(|el| {
                let src = el
                    .value()
                    .attr("src")
                    .ok_or(FromHtmlError::MissingImageUrl)?;
                Url::parse(src).map_err(FromHtmlError::InvalidImageUrl)
            })
            .collect::<Result<_, _>>()?;

        Ok(Self { images })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const MAID_CHAPTER_1: &str = include_str!("../../test_data/maid_chapter_1.html");

    #[test]
    fn parse_maid_1() {
        let html = Html::parse_document(MAID_CHAPTER_1);

        let chapter = Chapter::from_html(&html).expect("failed to parse chapter");
        dbg!(chapter);
    }
}
