use scraper::ElementRef;
use scraper::Html;
use scraper::Selector;
use url::Url;

lazy_static::lazy_static! {
    static ref A_SELECTOR: Selector = Selector::parse("a").expect("invalid a selector");
}

/// Error that may occur when parsing a [`Manga`] from html.
#[derive(Debug, thiserror::Error)]
pub enum FromHtmlError {
    /// Missing title
    #[error("missing title")]
    MissingTitle,

    /// Missing authors
    #[error("missing authors")]
    MissingAuthors,

    /// Missing Chapter List
    #[error("missing chapter list")]
    MissingChapterList,

    /// A chapter was invalid
    #[error("invalid chapter")]
    InvalidChapter(#[from] FromElementError),
}

/// A Manga
#[derive(Debug)]
pub struct Manga {
    /// The title
    pub title: String,
    /// The authors
    pub authors: Vec<String>,
    /// The chapter list
    pub chapter_list: Vec<Chapter>,
}

impl Manga {
    /// Make a [`Manga`] from a [`Html`].
    pub fn from_html(html: &Html) -> Result<Self, FromHtmlError> {
        lazy_static::lazy_static! {
            // static ref MANGA_INFO_TOP_SELECTOR: Selector = Selector::parse(".manga-info-top").expect("invalid manga info top selector");
            static ref MANGA_INFO_TEXT_LI_SELECTOR: Selector = Selector::parse(".manga-info-top > .manga-info-text > li").expect("invalid manga info text li selector");
            static ref H1_SELECTOR: Selector = Selector::parse("h1").expect("invalid h1 selector");
            static ref CHAPTER_LIST_SELECTOR: Selector = Selector::parse(".chapter-list").expect("invalid chapter list selector");
            static ref ROW_SELECTOR: Selector = Selector::parse(".row").expect("invalid row selector");
        }

        let mut manga_info_text_li_iter = html.select(&MANGA_INFO_TEXT_LI_SELECTOR);

        let title = manga_info_text_li_iter
            .next()
            .and_then(|el| el.select(&H1_SELECTOR).next()?.text().next())
            .ok_or(FromHtmlError::MissingTitle)?
            .to_string();

        let authors = manga_info_text_li_iter
            .next()
            .and_then(|el| {
                el.select(&A_SELECTOR)
                    .map(|el| el.text().next().map(|el| el.to_string()))
                    .collect::<Option<_>>()
            })
            .ok_or(FromHtmlError::MissingAuthors)?;

        let _status = manga_info_text_li_iter.next();

        let chapter_list = html
            .select(&CHAPTER_LIST_SELECTOR)
            .next()
            .map(|el| {
                el.select(&ROW_SELECTOR)
                    .map(Chapter::from_element)
                    .collect::<Result<_, _>>()
            })
            .ok_or(FromHtmlError::MissingChapterList)??;

        Ok(Self {
            title,
            authors,
            chapter_list,
        })
    }
}

#[derive(Debug, thiserror::Error)]
pub enum FromElementError {
    /// Missing title
    #[error("missing title")]
    MissingTitle,

    /// Missing url
    #[error("missing url")]
    MissingUrl,

    /// invalid url
    #[error("invalid url")]
    InvalidUrl(url::ParseError),

    /// Missing views_el
    #[error("missing views")]
    MissingViews,

    /// Invalid views
    #[error("invalid views")]
    InvalidViews(std::num::ParseIntError),

    /// Missing upload time
    #[error("missing upload time")]
    MissingUploadTime,
}

#[derive(Debug)]
pub struct Chapter {
    /// The title
    pub title: String,
    /// The url
    pub url: Url,
    /// The # of views
    pub views: u64,
    /// The upload time
    pub upload_time: String,
}

impl Chapter {
    /// Make a chapter from a html element
    pub fn from_element(element: ElementRef) -> Result<Self, FromElementError> {
        lazy_static::lazy_static! {
            static ref SPAN_SELECTOR: Selector = Selector::parse("span").expect("invalid span selector");
        }

        let mut span_iter = element.select(&SPAN_SELECTOR);

        let title_el = span_iter
            .next()
            .and_then(|el| el.select(&A_SELECTOR).next())
            .ok_or(FromElementError::MissingTitle)?;

        let title = title_el
            .text()
            .next()
            .ok_or(FromElementError::MissingTitle)?
            .to_string();

        let url = title_el
            .value()
            .attr("href")
            .map(Url::parse)
            .ok_or(FromElementError::MissingUrl)?
            .map_err(FromElementError::InvalidUrl)?;

        let views = span_iter
            .next()
            .and_then(|el| el.text().next())
            .ok_or(FromElementError::MissingViews)?
            .chars()
            .filter(|c| c.is_digit(10))
            .collect::<String>()
            .parse()
            .map_err(FromElementError::InvalidViews)?;

        let upload_time = span_iter
            .next()
            .and_then(|el| el.value().attr("title"))
            .ok_or(FromElementError::MissingUploadTime)?
            .to_string();

        Ok(Chapter {
            title,
            url,
            views,
            upload_time,
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const DUNGEON_KURASHI_NO_MOTO_YUUSHA: &str =
        include_str!("../../test_data/dungeon_kurashi_no_moto_yuusha.html");

    #[test]
    fn parse_dungeon_kurashi_no_moto_yuusha() {
        let html = Html::parse_document(DUNGEON_KURASHI_NO_MOTO_YUUSHA);
        let manga = Manga::from_html(&html).expect("failed to parse");
        dbg!(manga);
    }
}
